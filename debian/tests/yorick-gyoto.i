#!/usr/bin/yorick -i

// ensure mkdirp is defined
#include "pathfun.i"

// get or set a few variables
SRC_ROOT=cd(".");
AUTOPKGTEST_TMP=get_env("AUTOPKGTEST_TMP");
GYOTO_EXAMPLES_DIR=SRC_ROOT+"doc/examples/";
GYOTO_CHECK_NODISPLAY="true"

// no X11 display
batch, 1;
__xytitles=xytitles; __fma=fma; __winkill=winkill; __pli=pli; __plg=plg;
__pause=pause; __window=window;
xytitles = fma = winkill = pli = plg = pause = window = noop;

// get scripts
mkdirp, AUTOPKGTEST_TMP;
cd, AUTOPKGTEST_TMP;
system, "cp -f " + SRC_ROOT +"yorick/*.i ./";

// run upstream test suite
check_i=get_env("ARCH_DEFAULT_MPI_IMPL")?"check-mpi.i":"check.i";
include, check_i;

quit;
